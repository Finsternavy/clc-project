<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
	  xmlns:f="http://java.sun.com/jsf/core"
	  xmlns:h="http://java.sun.com/jsf/html">
<head>
</head>
<body>
<ui:composition template="/layouts/commonLayout.xhtml">
		
		<ui:define name="content">
			<div align="center">
				<h1>Member Login</h1>
				<form action="<%= request.getContextPath() %>/login" method="post">
					<table style="with: 80%">
						<tr>
							<td>UserName</td>
							<td><input type="text" name="userName" /></td>
						</tr>
						<tr>
							<td>Password</td>
							<td><input type="password" name="password" /></td>
						</tr>
					</table>
					<input type="submit" value="Submit"/>
				</form>
			
			</div>
		</ui:define>
		
</ui:composition>
	
</body>
</html>