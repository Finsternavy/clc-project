<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE xhtml>
<html xmlns="http://www.w3.org/1999/xhtml" 
	  xmlns:ui="http://java.sun.com/jsf/facelets"
	  xmlns:f="http://java.sun.com/jsf/core"
	  xmlns:h="http://java.sun.com/jsf/html">
<head>
<meta charset="ISO-8859-1">
<title>Project Manager Home</title>
</head>
<body>
<ui:composition template="/layouts/commonLayout.xhtml">
		
		<ui:define name="content">
			<div align="center">
				<h1>Logged in Successfully!</h1>
				<form action="<%= request.getContextPath() %>/myProjects" method="post">
				
					<h:button type="Submit" value = "Submit" />
					<input type="submit" value="Submit"/>
				</form>
			
			</div>
		</ui:define>
		
	</ui:composition>
</body>
</html>