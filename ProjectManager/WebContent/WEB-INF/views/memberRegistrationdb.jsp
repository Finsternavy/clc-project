<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div align="center">
		<h1>Member Registration Form</h1>
		<form action="<%= request.getContextPath() %>/register" method="post">
			<table style="with: 80%">
				<tr>
					<td>First Name</td>
					<td><input type="text" name="firstName" /></td>
				</tr>
				<tr>
					<td>Last Name</td>
					<td><input type="text" name="lastName" /><td>
				</td>
				<tr>
					<td>User Name</td>
					<td><input type="text" name="userName" /><td>
				</td>
				<tr>
					<td>Password</td>
					<td><input type="text" name="password" /><td>
				</td>
				<tr>
					<td>Email</td>
					<td><input type="text" name="email" /><td>
				</td>
				<tr>
					<td>Phone</td>
					<td><input type="text" name="phone" /><td>
				</td>			
			</table>
			<input type="submit" value="Submit"/>
		</form>
	</div>
</body>
</html>