<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
	  xmlns:f="http://java.sun.com/jsf/core"
	  xmlns:h="http://java.sun.com/jsf/html">
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<ui:composition template="/layouts/commonLayout.xhtml">
		
		<ui:define name="content">
			<div align="center">
				<h1>My Home Screen</h1>
				<form action="<%= request.getContextPath() %>/myProjects" method="post">
					<table style="with: 80%">
						<tr>
							<td>New Project Name</td>
							<td><input type="text" name="projectName" /></td>
						</tr>
						<tr>
							<td>Existing Projects</td>
							<td>To be added later</td>
						</tr>
					</table>
					<input type="submit" value="Create Project"/>
				</form>
			
			</div>
		</ui:define>
		
</ui:composition>
</body>
</html>