package model;

import java.util.Date;

public class Task {

	public Task() {
		
	}
	
	
	
	public Task(String name, String description, Date dueDate, Member memberAssigned, Boolean approved) {
		super();
		this.name = name;
		this.description = description;
		this.dueDate = dueDate;
		this.memberAssigned = memberAssigned;
		this.approved = approved;
	}



	private String name;
	private String description;
	private Date dueDate;
	private Member memberAssigned;
	private Boolean approved = false;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public Member getMemberAssigned() {
		return memberAssigned;
	}
	public void setMemberAssigned(Member memberAssigned) {
		this.memberAssigned = memberAssigned;
	}
	public Boolean getApproved() {
		return approved;
	}
	public void setApproved(Boolean approved) {
		this.approved = approved;
	}
	
	
	
}
