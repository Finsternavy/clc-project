package model;

import java.util.HashMap;
import java.util.Map;

import controller.MemberServlet;

public class Database {

	public Database() {
		
		
		
	}
	
	int keyValue = 0;
	
	public static HashMap<String, Member> members = new HashMap<String, Member>();
	
	public int keyValue() {
		return this.keyValue;
	}
	
	public void setKeyValue(int key) {
		this.keyValue = key;
	}
	
	public boolean validateIsNew(Member member) {
		
		String checkUserName = member.getUserName();
		
		boolean isNew = true;
		
		for(String s: members.keySet()) {
			String memberUserName = members.get(s).getUserName();
			if(memberUserName.equals(checkUserName)) {
			
				isNew = false;
				return isNew;
				
			}
		}
		
		return isNew;
	}
	
	public void registerMember(Member member) {
		
		this.keyValue = this.keyValue + 1;
		
		String keyString = String.valueOf(this.keyValue);
	
		members.put(keyString, member);
	}
	
	public void removeMember(String UserNameToBeRemoved) {
		
		boolean userFound = false;
		
		for (String s: members.keySet()) {
			String userName = members.get(s).getUserName();
			if(userName.equals(UserNameToBeRemoved)) {
				userFound = true;
				members.remove(s);
			}
		}
		
		if(userFound) {
			System.out.println("User was removed!");
		}else {
			
			System.out.println("UserName not found! No members were removed!");
		}
		
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(String s: members.keySet()) {
			sb.append("ID: " + s + " First Name: " + members.get(s).getFirstName() + " Last Name: " + members.get(s).getLastName() 
					+ " User Name: " + members.get(s).getUserName() + " Password: " + members.get(s).getPassword() + " Email: " +  
					members.get(s).getEmail() + " Phone: " + members.get(s).getPhone() + "\n");
		}
		
		return sb.toString();
	}
}
