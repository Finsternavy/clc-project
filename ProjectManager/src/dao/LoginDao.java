package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import controller.LoginBean;
import controller.MemberServlet;
import model.Database;

//for use with mySQL
public class LoginDao {
	
	MemberServlet mserv = new MemberServlet();

	public boolean validate(LoginBean loginBean) throws ClassNotFoundException {
		
		boolean status = false;
		String message = "Incorrect User Name.  Please register or reload the page.";
		
		//For use with mySQL
		//Class.forName("com.mysql.cj.jdbc.Driver");
		
		String userName = loginBean.getUsername();
		String userPassword = loginBean.getPassword();
		
		for(String s: Database.members.keySet()) {
			
			String checkUserName = Database.members.get(s).getUserName();
			String checkUserPassword = Database.members.get(s).getPassword();
			if(checkUserName.equals(userName)) {
				
				if(checkUserPassword.equals(userPassword)) {
					
					message = "User name and password match found!";
					System.out.println(message);
					
					return true;
					
				}else {
					message = "Incorrect Password";
					System.out.println(message);
					return false;
					
				}
				
			}
			
		}
		
		System.out.println(message);
		
		//For use with mySQL
		/*try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/members", "root", "QAZXSWedcvfr1!");
				
				PreparedStatement preparedStatement = connection.prepareStatement("select * from member where username = ? and password = ?")){
				preparedStatement.setString(1, loginBean.getUsername());
				preparedStatement.setString(2, loginBean.getPassword());
				
				System.out.println(preparedStatement);
				ResultSet rs = preparedStatement.executeQuery();
				status = rs.next();
				
		} catch (SQLException e) {
			
			e.printStackTrace();
		}*/
						
		
		return status;
	}
}
