package dao;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.Member;

public class MemberDao {

	public int registerMember(Member member) throws ClassNotFoundException {
		
		String INSERT_USER_SQL = "INSERT INTO member" + 
				"(id, first_name, last_name, username, password, email, phone) VALUES" +
				"(?, ?, ?, ?, ?, ?, ?);";
		
		int result = 0;
		
		Class.forName("com.mysql.cj.jdbc.Driver");
		
			try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/members", "root", "QAZXSWedcvfr1!");
				
				PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER_SQL)){
				
				//Prevent duplicating keys
				Statement stmt = connection.createStatement();
				String query = "select count(*) from member";
			      ResultSet rs = stmt.executeQuery(query);
			      rs.next();
			      int count = rs.getInt(1) + 1;
			      
				preparedStatement.setInt(1, count);
				preparedStatement.setString(2,  member.getFirstName());
				preparedStatement.setString(3,  member.getLastName());
				preparedStatement.setString(4,  member.getUserName());
				preparedStatement.setString(5,  member.getPassword());
				preparedStatement.setString(6,  member.getEmail());
				preparedStatement.setString(7,  member.getPhone());
				
				
				System.out.println(preparedStatement);
				
				result = preparedStatement.executeUpdate();
				
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		return result;
	}
}
