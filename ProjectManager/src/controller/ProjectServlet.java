package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Project;
import model.ProjectsManager;

/**
 * Servlet implementation class ProjectServlet
 */
@WebServlet("/myProjects")
public class ProjectServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProjectServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    ProjectsManager myProjects = new ProjectsManager();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String projectName = request.getParameter("projectName");
		
		Project newProject = new Project(projectName);
		
		myProjects.myProjects.add(newProject);
		//Do something with the new project. place in list to be displayed somewhere.

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/myProjects.jsp");
		dispatcher.forward(request, response);
			
	}

}
