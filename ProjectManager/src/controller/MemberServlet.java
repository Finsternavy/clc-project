package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.MemberDao;
import model.Database;
import model.Member;

/**
 * Servlet implementation class MemberServlet
 */
@WebServlet("/register")
public class MemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private MemberDao memberDao = new MemberDao();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MemberServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Database myDB = new Database();
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/memberRegistrationdb.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/*
		 * 
		 * Need to add code to ensure user does not already have an account active.
		 * 
		 */
		
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		
		Member member = new Member(firstName, lastName, userName, password, email, phone);
		
		if(myDB.validateIsNew(member)) {
			myDB.registerMember(member);
			System.out.println(myDB.toString());
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/logindb.jsp");
			dispatcher.forward(request, response);
			
		}else {
			System.out.println("User name already in use. Please choose a different user name!");
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/memberRegistrationdb.jsp");
			dispatcher.forward(request, response);
		}
		
		/* Used with mySQL
		try {
			memberDao.registerMember(member);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
			
	}

}
